using System.Windows.Media;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Utils
{
    public class FtthExportToExcelCommand : DefaultExportToExcelCommand
    {
        protected override ExcelExportingOptions ExportingOptions
        {
            get
            {
                var options = base.ExportingOptions;
                options.ExcludeColumns.Add("IsChecked");

                options.ExportingEventHandler = (sender, args) =>
                {
                    if (args.CellType == ExportCellType.StackedHeaderCell)
                    {
                        args.CellStyle.BackGroundBrush = new SolidColorBrush(Colors.CornflowerBlue);
                        args.CellStyle.ForeGroundBrush = new SolidColorBrush(Colors.WhiteSmoke);
                        args.CellStyle.FontInfo.Bold = true;
                        args.Handled = true;
                    }

                    if (args.CellType == ExportCellType.HeaderCell)
                    {
                        args.CellStyle.BackGroundBrush = new SolidColorBrush(Colors.CornflowerBlue);
                        args.CellStyle.ForeGroundBrush = new SolidColorBrush(Colors.WhiteSmoke);
                        args.CellStyle.FontInfo.Bold = true;
                        args.Handled = true;
                    }

                    if (args.CellType == ExportCellType.TableSummaryCell)
                    {
                        args.CellStyle.BackGroundBrush = new SolidColorBrush(Colors.CornflowerBlue);
                        args.CellStyle.ForeGroundBrush = new SolidColorBrush(Colors.WhiteSmoke);
                        args.CellStyle.FontInfo.Bold = true;
                        args.Handled = true;
                    }
                };

                options.CellsExportingEventHandler = (sender, args) =>
                {
                    if (args.CellType == ExportCellType.HeaderCell || args.CellType == ExportCellType.StackedHeaderCell)
                    {
                        args.Range.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
                        args.Range.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
                        args.Range.BorderAround(ExcelLineStyle.Medium);
                    }

                    if (args.CellType == ExportCellType.TableSummaryCell)
                    {
                        args.Range.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
                    }
                };

                return options;
            }
        }
    }
}