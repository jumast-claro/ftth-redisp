using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Utils
{
    public class ExportToExcelSaveFileDialog
    {
        private SaveFileDialog _dialog;
        private bool _dialogResult;

        private SaveFileDialog createSaveFileDialog()
        {
            var saveFileDialog = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                RestoreDirectory = true,
                FilterIndex = 2,
                Filter =
                    "Excel 97 to 2003 Files(*.xls)|*.xls|Excel 2007 to 2010 Files(*.xlsx)|*.xlsx|Excel 2013 File(*.xlsx)|*.xlsx"
            };
            return saveFileDialog;
        }

        public bool DialogResult => _dialogResult;

        public void ShowDialog()
        {
            this._dialog = this.createSaveFileDialog();
            if (_dialog.ShowDialog() == true)
            {
                this._dialogResult = true;
            }
            else
            {
                _dialogResult = false;
            }
        }

        public string FileName => _dialog.FileName;

        public Stream OpenFile()
        {
            return _dialog.OpenFile();
        }

        public ExcelVersion ExcelVersion
        {
            get
            {
                ExcelVersion version;
                switch (_dialog.FilterIndex)
                {
                    case 1:
                        version = ExcelVersion.Excel97to2003;
                        break;
                    case 2:
                        version = ExcelVersion.Excel2010;
                        break;
                    default:
                        version = ExcelVersion.Excel2013;
                        break;
                }

                return version;
            }
        }
    }
}