using System.Threading.Tasks;
using System.Windows.Input;

namespace Jumast.Claro.Redisp.WpfApp
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}