using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Utils
{
    
    public class ExcelExporter
    {
        private readonly ExcelExportingOptions _exportingOptions;
        private readonly SfDataGrid _sfDataGrid;

//        public ExcelExporter(SfDataGrid sfDataGrid)
//        {
//            _sfDataGrid = sfDataGrid;
//
//            var options = new ExcelExportingOptions();
//            options.ExcelVersion = ExcelVersion.Excel2013;
//            options.ExportStackedHeaders = true;
//            this._exportingOptions = options;
////            this.ExportingOptions = options;
//        }

        public ExcelExporter(SfDataGrid sfDataGrid, ExcelExportingOptions exportingOptions)
        {
            _sfDataGrid = sfDataGrid;
            _exportingOptions = exportingOptions;
        }

        public void ExportAs(string fileName, ExcelVersion version)
        {
            var dg = this._sfDataGrid;
            var excelEngine = dg.ExportToExcel(dg.View, this._exportingOptions);
            var workBook = excelEngine.Excel.Workbooks[0];
            workBook.Version = this.WorkbookVersion;

            var range = "A" + (dg.StackedHeaderRows.Count + 1).ToString() + ":" +
                        workBook.Worksheets[0].UsedRange.End.AddressLocal;
            excelEngine.Excel.Workbooks[0].Worksheets[0].AutoFilters.FilterRange = workBook.Worksheets[0].Range[range];
            workBook.SaveAs(fileName);
        }

        public ExcelVersion WorkbookVersion { get; set; } = ExcelVersion.Excel2013;

//        public ExcelExportingOptions ExportingOptions { get; set; }
    }
}