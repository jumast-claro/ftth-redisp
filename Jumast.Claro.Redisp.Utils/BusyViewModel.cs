using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Jumast.Claro.Redisp.Utils
{
    public sealed class BusyViewModel : INotifyPropertyChanged
    {
        public BusyViewModel(string text)
        {
            Text = text;
        }
        
        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Text { get; }


//        private bool _isBusy = false;
//
//        public bool IsBusy
//        {
//            get => _isBusy;
////            set
////            {
////                _isBusy = value;
////                OnPropertyChanged();
////            }
//        }
    }
}