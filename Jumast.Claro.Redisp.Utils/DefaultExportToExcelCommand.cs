using System;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Jumast.Claro.Redisp.WpfApp;
using Microsoft.Win32;
using Syncfusion.Data;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Utils
{
    public class DefaultExportToExcelCommand : AsyncCommandBase
    {
        public DefaultExportToExcelCommand()
        {
        }

        protected virtual ExcelExportingOptions ExportingOptions
        {
            get
            {
                var options = new ExcelExportingOptions();
                options.ExcelVersion = ExcelVersion.Excel2013;
                options.ExportStackedHeaders = true;
                return options;
            }
        }

        public override bool CanExecute(object parameter)
        {
            return parameter is SfDataGrid;
        }

        private void _execute(SfDataGrid sfDataGrid)
        {
            var exporter = new ExcelExporter(sfDataGrid, this.ExportingOptions);
            var saveFileDialog = new ExportToExcelSaveFileDialog();
            saveFileDialog.ShowDialog();
            if (saveFileDialog.DialogResult != true) return;

            var excelVersion = saveFileDialog.ExcelVersion;
            var fileName = saveFileDialog.FileName;

            var busyIndicator = new BusyIndicator();
            busyIndicator.Show();
            exporter.ExportAs(fileName, excelVersion);
            busyIndicator.Close();

            askOpenWorkbook(fileName);
        }

        private void askOpenWorkbook(string fileName)
        {
            if (MessageBox.Show("Querés abrir el libro?", "Se creó el libro",
                    MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start(fileName);
            }
        }


        public override Task ExecuteAsync(object parameter)
        {
            var sfDataGrid = (SfDataGrid) parameter;

            var task = Task.Run((() =>
            {
                try
                {
                    var dispatcher = sfDataGrid.Dispatcher;
                    dispatcher.InvokeAsync((() => { this._execute(sfDataGrid); }));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }));

            return task;
        }
    }
}