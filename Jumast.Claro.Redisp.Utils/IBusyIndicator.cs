using System.Windows;

namespace Jumast.Claro.Redisp.Utils
{
    public interface IBusyIndicator
    {
        void Close();
        void Show();
    }
}