using System.Threading;
using System.Windows;

namespace Jumast.Claro.Redisp.Utils
{
    public class BusyIndicator : IBusyIndicator
    {
        private Window _window;

        public BusyIndicator()
        {
//            this.createWindow();
        }

        private Window createWindow()
        {
            var busyView = new BusyView();
            var busyViewModel = new BusyViewModel("Creando archivo excel...");
            busyView.DataContext = busyViewModel;

            var window = new Window();
            window.Content = busyView;
            window.WindowStyle = WindowStyle.ToolWindow;
            window.SizeToContent = SizeToContent.WidthAndHeight;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.WindowStyle = WindowStyle.None;
            window.ResizeMode = ResizeMode.NoResize;
            window.ShowInTaskbar = false;
            window.ShowInTaskbar = false;

            _window = window;
            return window;
        }

        public void Close()
        {
            _window.Dispatcher.Invoke(_window.Close);
        }

        public void Show()
        {
            Thread thread = new Thread(() =>
            {
                var busyIndicator = this.createWindow();
                busyIndicator.Closed += (sender2, e2) => busyIndicator.Dispatcher.InvokeShutdown();
                busyIndicator.Show();

                System.Windows.Threading.Dispatcher.Run();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }
    }
}