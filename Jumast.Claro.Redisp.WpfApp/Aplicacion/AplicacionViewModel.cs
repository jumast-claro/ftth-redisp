﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Jumast.Claro.Redisp.Data;
using Jumast.Claro.Redisp.Utils;
using Jumast.Claro.Redisp.WpfApp.Login;
using Syncfusion.Data;
using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation;
using CollectionViewExtensions = Syncfusion.Data.CollectionViewExtensions;

namespace Jumast.Claro.Redisp.WpfApp
{
    public sealed class AplicacionViewModel : INotifyPropertyChanged
    {
        private readonly ViewModelProvider _viewModelProvider;

        private IEnumerable<SaturacionViewModel> _saturacion1 = new List<SaturacionViewModel>();
        private IEnumerable<SaturacionViewModel> _saturacion2 = new List<SaturacionViewModel>();

        private IEnumerable<EjeRedFtthViewModel> _registros;

        public AplicacionViewModel(IReporteSaturacion reporteSaturacion, TablaRedesFtth tablaRedesFtth)
        {
            this._viewModelProvider = new ViewModelProvider(reporteSaturacion, tablaRedesFtth);
        }

        private void load2()
        {
            var dic = new Dictionary<string, SaturacionViewModel>();

            foreach (var viewModel in this._viewModelProvider)
            {
                if (viewModel.TipoDeRed !=2 ) continue;

                var red = viewModel.Red;
                SaturacionViewModel satViewModel = null;

                if (dic.ContainsKey(red))
                {
                    satViewModel = dic[red];
                }
                else
                {
                    satViewModel = new SaturacionViewModel();
                    satViewModel.Red = red;
                    dic[red] = satViewModel;
                }


                if (viewModel.Estado == "DISPONIBLE")
                {
                    satViewModel.CantidadDisponibles++;
                }
                else
                {
                    satViewModel.CantidadSaturados++;
                }
            }

            this._saturacion2 = dic.Values.OrderBy(r => r.Red);
        }

        private void load1()
        {
            var dic = new Dictionary<string, SaturacionViewModel>();

            foreach (var viewModel in this._viewModelProvider)
            {
                if (viewModel.TipoDeRed != 1) continue;

                var red = viewModel.Red;
                SaturacionViewModel satViewModel = null;

                if (dic.ContainsKey(red))
                {
                    satViewModel = dic[red];
                }
                else
                {
                    satViewModel = new SaturacionViewModel();
                    satViewModel.Red = red;
                    dic[red] = satViewModel;
                }


                if (viewModel.Estado == "DISPONIBLE")
                {
                    satViewModel.CantidadDisponibles++;
                }
                else
                {
                    satViewModel.CantidadSaturados++;
                }
            }

            this._saturacion1 = dic.Values.OrderBy(r => r.Red);
        }

        public void Load()
        {
            this.load1();
            this.load2();
            this._registros = _viewModelProvider.ToList();
        }

        public IEnumerable<EjeRedFtthViewModel> Registros => this._registros;

        public IEnumerable<SaturacionViewModel> SaturacionRed1 => _saturacion1;

        public int Chart1Height => (SaturacionRed1.Count() * 35) + 200;
        public int Chart2Height => (SaturacionRed2.Count() * 35) + 200;

        public IEnumerable<SaturacionViewModel> SaturacionRed2 => this._saturacion2;

        private bool _isBusy = false;

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}