﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.Redisp.WpfApp.Login
{
    public class StringBindableProperty : INotifyPropertyChanged
    {

        public StringBindableProperty()
        {
            _displayString = string.Empty;
        }

        private string _displayString;
        public string DisplayString
        {
            get => _displayString;
            set
            {
                _displayString = value;
                OnPropertyChanged();
            }
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
