﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Jumast.Claro.Redisp.WpfApp.Login
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class LoginViewModel : INotifyPropertyChanged
    {
        private readonly ILogingValidator _logingValidator;
        public event EventHandler LoginSucceded;

        public LoginViewModel(ILogingValidator logingValidator)
        {
            _logingValidator = logingValidator;
            UserNameProperty = new StringBindableProperty();
            IngresarCommand = new RelayCommand(_ingresarExecuted, _ingresarCanExecute);
        }

        protected virtual void OnLoginSucceded(EventArgs e)
        {
            LoginSucceded?.Invoke(this, e);
        }

        public StringBindableProperty UserNameProperty { get; }

        public ICommand IngresarCommand { get; }

        private async void _ingresarExecuted(object o)
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                var passwordBox = (PasswordBox) o;
                bool isValidUser =
                    _logingValidator.ValidateCredentials(UserNameProperty.DisplayString, passwordBox.Password);
                if (isValidUser)
                {
                    OnLoginSucceded(EventArgs.Empty);
                    IsBusy = false;
                }

                else
                {
                    IsBusy = false;
                    MessageBox.Show("Credenciales inválidas.");
                }
            });
        }

        private bool _ingresarCanExecute(object o)
        {
            return UserNameProperty.DisplayString != string.Empty;
        }

        //---------------------------------------------------------------------
        // INotifyPropertyChanged
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool _isBusy = false;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }
    }
}