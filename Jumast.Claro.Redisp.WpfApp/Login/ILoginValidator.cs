﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.Redisp.WpfApp.Login
{
    public interface ILogingValidator
    {
        bool ValidateCredentials(string userName, string password);
    }
}
