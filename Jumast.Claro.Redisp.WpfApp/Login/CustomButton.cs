﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class CustomButton : System.Windows.Controls.Button
    {
        public static readonly DependencyProperty MouseOverBackgroundProperty = DependencyProperty.Register("MouseOverBackground", typeof(Brush), typeof(CustomButton), new FrameworkPropertyMetadata(Brushes.DarkMagenta, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public Brush MouseOverBackground
        {
            get => (Brush)GetValue(MouseOverBackgroundProperty);
            set => SetValue(MouseOverBackgroundProperty, value);
        }
    }
}
