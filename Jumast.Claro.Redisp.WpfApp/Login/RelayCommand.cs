﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Jumast.Claro.Redisp.WpfApp.Login
{
    public class RelayCommand : ICommand
    {
        //---------------------------------------------------------------------
        // Events
        //---------------------------------------------------------------------
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }

        }

        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }
            _execute = execute;
            _canExecute = canExecute;
        }

        //---------------------------------------------------------------------
        // Properties
        //---------------------------------------------------------------------
        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
