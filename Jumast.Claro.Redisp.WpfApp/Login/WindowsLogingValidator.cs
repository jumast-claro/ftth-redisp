﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.Windows;

namespace Jumast.Claro.Redisp.WpfApp.Login
{
    public class WindowsLogingValidator : ILogingValidator
    {
        public bool ValidateCredentials(string userName, string password)
        {
            var isValidUser = false;

            try
            {
                using (var context = new PrincipalContext(ContextType.Domain))
                {
                    isValidUser = context.ValidateCredentials(userName, password);
                    return isValidUser;
                }
            }
            catch (Exception e)
            {
                using (var context = new PrincipalContext(ContextType.Machine))
                {
                    isValidUser = context.ValidateCredentials(userName, password);
                    return isValidUser;
                }
            }

           

        }
    }
}
