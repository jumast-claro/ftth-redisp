using System;
using System.IO;
using System.Windows;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class LoginLogger
    {
        private readonly string _fullFileNameWithExtension;

        public LoginLogger(string fullFileNameWithExtension)
        {
            if (!File.Exists(fullFileNameWithExtension))
            {
                var msg = "No se pudo encontrar el arhivo " + fullFileNameWithExtension;
                throw new FileNotFoundException(msg);
            }
            _fullFileNameWithExtension = fullFileNameWithExtension;
        }

        public void LogExit()
        {
            log("Exit   ");
        }

        public void LogStartUp()
        {
            log("StartUp");
        }

        private void log(string escripcion)
        {
            var user = Environment.UserName;

            try
            {
                using (FileStream fs = new FileStream(this._fullFileNameWithExtension, FileMode.Append))
                {
                    using (TextWriter tw = new StreamWriter(fs))
                    {
                        tw.WriteLine(user + "," + escripcion + "," + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}