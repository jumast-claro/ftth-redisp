﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Jumast.Claro.Redisp.Data;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class ViewModelProvider : IEnumerable<EjeRedFtthViewModel>
    {
        private readonly IReporteSaturacion _reporteSaturacion;
        private readonly TablaRedesFtth _tablaRedesFtth;

        public ViewModelProvider(IReporteSaturacion reporteSaturacion, TablaRedesFtth tablaRedesFtth)
        {
            _reporteSaturacion = reporteSaturacion;
            _tablaRedesFtth = tablaRedesFtth;
        }

        public IEnumerator<EjeRedFtthViewModel> GetEnumerator()
        {
            var ejes = new List<EjeRedFtthViewModel>();

            var i = 1;
            foreach (var registroSaturacion in _reporteSaturacion)
            {
                var extractoSplitter = registroSaturacion.Splitters.Value.Substring(0, 4);
                var red = _tablaRedesFtth.FirstOrDefault(e => e.ExtractoSp.Value == extractoSplitter.ToUpper());

                if (red == null)
                {
                    red = new NullRegistroDeRedFtth();
                }


                var viewModel = new EjeRedFtthViewModel(registroSaturacion, red);
                viewModel.RowNumber = i;
                i++;

                ejes.Add(viewModel);
            }

            return ejes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}