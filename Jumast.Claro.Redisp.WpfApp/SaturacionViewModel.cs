﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class SaturacionViewModel
    {

        public string Red { get; set; }
        public int CantidadDisponibles { get; set; }
        public int CantidadSaturados { get; set; }
        public int Total => CantidadDisponibles + CantidadSaturados;

        public double PorcentajeDisponibles => (CantidadDisponibles / (double)Total) * 100;
        public double PorcentajeSaturados => (CantidadSaturados / (double)Total) * 100;

        public double DisponiblesWidth => PorcentajeDisponibles * 3.5;
        public double SaturadosWidth => PorcentajeSaturados * 3.5;

        public string Text => PorcentajeDisponibles.ToString("F") + "%";

    }
}
