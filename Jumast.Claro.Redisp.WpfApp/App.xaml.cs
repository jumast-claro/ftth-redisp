﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Syncfusion.Windows.Controls;

namespace Jumast.Claro.Redisp.WpfApp
{
    public partial class App : Application
    {
        private readonly LoginLogger _logger;

        public App()
        {
            try
            {
                this._logger = new LoginLogger(@"S:\Implantacion\Servicios Fijos\Servicios Fijos\Mantenimiento red FTTH\Ampliaciones\CLARO_FTTH_REDISP\Application Data\log.txt");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Application.Current.Shutdown();
            }
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            _logger.LogExit();
        }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            _logger.LogStartUp();
        }
    }
}