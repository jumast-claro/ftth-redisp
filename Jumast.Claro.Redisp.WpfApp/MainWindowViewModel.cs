﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Jumast.Claro.Redisp.Data;
using Jumast.Claro.Redisp.WpfApp.Login;
using Syncfusion.UI.Xaml.Charts;

namespace Jumast.Claro.Redisp.WpfApp
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {

        private object _currentViewModel;

        public MainWindowViewModel()
        {
            
            //var logger = new LoginLogger(@"S:\Implantacion\Servicios Fijos\Servicios Fijos\Mantenimiento red FTTH\Ampliaciones\CLARO_FTTH_REDISP\Application Data\log.txt");
            
            var validator = new WindowsLogingValidator();
            var logingViewModel = new LoginViewModel(validator);
            logingViewModel.UserNameProperty.DisplayString = Environment.UserName;

            IReporteSaturacion reporteSaturacion = new ExcelReporteSaturacion(@"S:\Implantacion\Servicios Fijos\Servicios Fijos\Mantenimiento red FTTH\Ampliaciones\CLARO_FTTH_REDISP\reporte.xls");
            TablaRedesFtth redes = new TablaRedesFtth(@"S:\Implantacion\Servicios Fijos\Servicios Fijos\Mantenimiento red FTTH\Ampliaciones\CLARO_FTTH_REDISP\FTTH_REDISP_mapeo.xlsx");
            AplicacionViewModel aplicacionViewModel = new AplicacionViewModel(reporteSaturacion, redes);
            logingViewModel.LoginSucceded += async (sender, args) =>
            {
                await Task.Run((() =>
                {
                    IsBusy = true;
                    try
                    {
                        aplicacionViewModel.Load();
                        IsBusy = false;
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        Application.Current.Shutdown();
                    }
                   

                }));
              
                CurrentViewModel = aplicacionViewModel;

            };

            _currentViewModel = logingViewModel;
      
        }

        public object CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        //------------------------------------------------------ 
        //  INotifyPropertyChanged
        //------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

       

        private bool _isBusy = false;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }


        private WindowState _windowState = WindowState.Maximized;
        public WindowState WindowState
        {
            get => _windowState;
            set
            {
                _windowState = value; 
                OnPropertyChanged();
            }
        }
    }
}
