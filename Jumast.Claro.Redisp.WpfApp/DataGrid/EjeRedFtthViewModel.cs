﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumast.Claro.Redisp.Data;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class EjeRedFtthViewModel
    {
        public EjeRedFtthViewModel(IRegistroSaturacion registroSaturacion, IRegistroRedFtth registroRed)
        {
            this.Tipo = registroSaturacion.Tipo.Value;
            this.Direccion = registroSaturacion.Direccion.Value;
            this.IdEje = registroSaturacion.IdEje.Value;
            this.Estado = registroSaturacion.Estado.Value;
            this.Puertos = registroSaturacion.Puertos.Value;
            this.Cantidad = registroSaturacion.Cantidad.Value;
            this.Splitters = registroSaturacion.Splitters.Value;
            this.Olt = registroSaturacion.Olt.Value;
            this.ExtractoSp = registroRed.ExtractoSp.Value;
            this.Red = registroRed.Red.Value;
            this.TipoDeRed = registroRed.TipoDeRed.Value;
            this.IsChecked = true;
        }

        public string Tipo { get; private set; }
        public string Direccion { get; private set; }
        public string IdEje { get; private set; }
        public string Estado { get; private set; }
        public int Puertos { get; private set; }
        public int Cantidad { get; private set; }
        public string Splitters { get; private set; }
        public string Olt { get; private set; }
        public string ExtractoSp { get; private set; }
        public string Red { get; private set; }
        public int TipoDeRed { get; private set; }
        
        public bool IsChecked { get; set; }
        
        public int RowNumber { get; set; }

    }
}
