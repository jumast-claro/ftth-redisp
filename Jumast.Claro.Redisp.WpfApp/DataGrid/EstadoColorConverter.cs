﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Jumast.Claro.Redisp.WpfApp
{
    public class EstadoColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var estado = (string) value;

            if (estado == "DISPONIBLE")
            {
                return new SolidColorBrush(Color.FromRgb(123, 207, 123));
            }
            if(estado == "SATURADO")
            {
                return new SolidColorBrush(Color.FromRgb(217, 122, 122));
            }
            return new SolidColorBrush(Colors.Transparent);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
