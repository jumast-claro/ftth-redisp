﻿using System.Dynamic;

namespace Jumast.Claro.Redisp.Data
{
    public interface IRegistroSaturacion
    {
        StringField Tipo { get; }
        StringField Direccion { get; }
        StringField IdEje { get; }
        StringField Estado { get; }
        IntField Puertos { get; }
        IntField Cantidad { get; }
        StringField Splitters { get; }
        StringField Olt { get; }
    }
}