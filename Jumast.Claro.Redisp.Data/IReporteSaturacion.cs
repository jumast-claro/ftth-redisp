﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumast.Claro.Redisp.Data
{
    public interface IReporteSaturacion : IEnumerable<IRegistroSaturacion>
    {
    }
}
