﻿using System;
using System.CodeDom;
using System.Diagnostics.CodeAnalysis;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation.PivotAnalysis;

namespace Jumast.Claro.Redisp.Data
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ExcelRegistroSaturacion : IRegistroSaturacion
    {
        private readonly IWorksheet _workSheet;
        private readonly int _rowNumber;
        private const int TIPO = 1;
        private const int DIRECCION = 2;
        private const int ID_EJE = 3;
        private const int ESTADO = 4;
        private const int PUERTOS = 5;
        private const int CANTIDAD = 6;
        private const int SPLITTERS = 7;
        private const int OLT = 8;

        public ExcelRegistroSaturacion(IWorksheet workSheet, int rowNumber)
        {
            _workSheet = workSheet;
            _rowNumber = rowNumber;


            this.Tipo = new StringField(this.getValue(TIPO));
            this.Direccion = new StringField(this.getValue(DIRECCION));
            this.IdEje = new StringField(this.getValue(ID_EJE));
            this.Estado = new StringField(this.getValue(ESTADO));
            this.Puertos = new IntField(this.getNumberAsInt(PUERTOS));
            this.Cantidad = new IntField(this.getNumberAsInt(CANTIDAD));
            this.Splitters = new StringField(this.getValue(SPLITTERS));
            this.Olt = new StringField(this.getValue(OLT));
        }

        public StringField Tipo { get; }
        public StringField Direccion { get; }
        public StringField IdEje { get; }
        public StringField Estado { get; }
        public IntField Puertos { get; }
        public IntField Cantidad { get; }
        public StringField Splitters { get; }
        public StringField Olt { get; }

        private string getValue(int col)
        {
            return _workSheet.Range[_rowNumber, col].Value;
        }

        private int getNumberAsInt(int columna)
        {
            var valor = this._workSheet.Range[this._rowNumber, columna].Value;
            return Int16.Parse(valor);
        }
    }

}
