﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Data
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ExcelReporteSaturacion : IReporteSaturacion
    {
        private readonly string _rutaCompletaConExtension;
        private const int HOJA = 0;
        private const int PRIMERA_FILA_CON_DATOS = 2;

        private List<ExcelRegistroSaturacion> _records;

        public ExcelReporteSaturacion(string rutaCompletaConExtension)
        {
            _rutaCompletaConExtension = rutaCompletaConExtension;
        }


        private void leerHoja(int hoja)
        {
            _records = new List<ExcelRegistroSaturacion>();

            ExcelEngine excelEngine = new ExcelEngine();
            IApplication application = excelEngine.Excel;
            IWorkbook wb = application.Workbooks.OpenReadOnly(_rutaCompletaConExtension);
            IWorksheet worksheet = wb.Worksheets[hoja];
            int cantidadDeFilas = worksheet.Rows.Length;

            for (var numeroDeFila = PRIMERA_FILA_CON_DATOS; numeroDeFila <= cantidadDeFilas; numeroDeFila++)
            {
                var excelRecord = new ExcelRegistroSaturacion(worksheet, numeroDeFila);
                _records.Add(excelRecord);
            }

        }

        public IEnumerator<IRegistroSaturacion> GetEnumerator()
        {
            if (this._records == null)
            {
                this.leerHoja(HOJA);
            }
            return this._records.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
       
    }
}
