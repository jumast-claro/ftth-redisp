﻿namespace Jumast.Claro.Redisp.Data
{
    public class StringField
    {
        private readonly string _value;

        public StringField(string value)
        {
            _value = value;
        }

        public string Value => this._value;
    }
}
