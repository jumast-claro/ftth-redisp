﻿using System;
using System.Globalization;
using Syncfusion.XlsIO;

namespace Jumast.Claro.Redisp.Data
{
    public class RegistroRedFtth : IRegistroRedFtth
    {
        private readonly IWorksheet _workSheet;
        private readonly int _rowNumber;
        private const int EXTRACTO_SP = 1;
        private const int RED = 2;
        private const int TIPO_DE_RED = 3;

        public RegistroRedFtth(IWorksheet workSheet, int rowNumber)
        {
            _workSheet = workSheet;
            _rowNumber = rowNumber;


            this.ExtractoSp = new StringField(this.getValue(EXTRACTO_SP));
            this.Red = new StringField(this.getValue(RED));
            this.TipoDeRed = new IntField(this.getNumberAsInt(TIPO_DE_RED));
        }

        public StringField ExtractoSp { get; }
        public StringField Red { get; }
        public IntField TipoDeRed { get; }

        private string getValue(int col)
        {
            return _workSheet.Range[_rowNumber, col].Value;
        }

        private int getNumberAsInt(int columna)
        {
            var valor = this._workSheet.Range[this._rowNumber, columna].Value;
            return (int) Double.Parse(valor.Replace(',', '.'), NumberStyles.AllowDecimalPoint);
        }
    }

    public interface IRegistroRedFtth
    {
        StringField ExtractoSp { get; }
        StringField Red { get; }
        IntField TipoDeRed { get; }
    }

    public class NullRegistroDeRedFtth : IRegistroRedFtth
    {
        public NullRegistroDeRedFtth()
        {
            ExtractoSp = new StringField("");
            Red = new StringField("");
            TipoDeRed = new IntField(0);
        }
        
        public StringField ExtractoSp { get; }
        public StringField Red { get; }
        public IntField TipoDeRed { get; }
    }
}