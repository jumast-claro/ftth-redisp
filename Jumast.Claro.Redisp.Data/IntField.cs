﻿namespace Jumast.Claro.Redisp.Data
{
    public class IntField
    {
        private readonly int _value;

        public IntField(int value)
        {
            _value = value;
        }

        public int Value => this._value;
    }
}